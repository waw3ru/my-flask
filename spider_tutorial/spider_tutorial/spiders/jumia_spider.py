import scrapy


class JumiaSpider(scrapy.Spider):
    name = "jumia"

    def start_requests(self):
        urls = [
            'https://www.jumia.co.ke/',
        ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        for main_categories in response.css('#menuFixed ul.menu-items li.menu-item'):
            main = {
                'Main Categories': main_categories.css('a.main-category span.nav-subTxt::text').extract_first(),
                'Main Categories Links': main_categories.css('a.main-category::attr("href")').extract_first(),
                'categories': []
                # 'Categories': main_categories.css('div.navLayerWrapper div.submenu .column div.categories a.category::text').extract(),
                # 'Sub-Categories': main_categories.css('div.navLayerWrapper div.submenu .column div.categories a.subcategory::text').extract(),
                # 'Categories Links': main_categories.css('div.navLayerWrapper div.submenu .column div.categories a.category::attr("href")').extract(),
                # 'Sub-Category Links': main_categories.css('div.navLayerWrapper div.submenu .column div.categories a.subcategory::attr("href")').extract(),
            }
        
            for categories in main_categories.css('div.navLayerWrapper div.submenu .column'):

                for sub_categories in categories.css('div.categories'):
                    category = {
                        'Category': sub_categories.css('.category::text').extract_first(),
                        'Category Links': sub_categories.css('a.category::attr("href")').extract_first(),
                        'Sub Categories': sub_categories.css('a.subcategory::text').extract(),
                        'Sub Category inks': sub_categories.css('a.subcategory::attr("href")').extract()
                    }

                    main['categories'].append(category)

            
            yield main

        